package server.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class ServerController {
	
	@GetMapping("work")
	public String testSample() {
		return "hello test";
	}
	
	@GetMapping("exception")
	public String testException() throws Exception {
		throw new Exception("There is my eception");
	}

}
